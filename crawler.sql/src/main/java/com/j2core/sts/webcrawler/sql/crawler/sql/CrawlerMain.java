package com.j2core.sts.webcrawler.sql.crawler.sql;

import com.j2core.sts.webcrawler.sql.dao.sql.PropertyLoaderSingleton;
import com.j2core.sts.webcrawler.sql.dao.sql.sqldao.SQLDBWorker;
import org.apache.log4j.Logger;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.util.Properties;

/**
 * The main class for start web crawler_sql
 */
public class CrawlerMain {


    private final static Logger LOGGER = Logger.getLogger(CrawlerMain.class);         // class for save logs information
    private final static int amountMilliSecondDeprecate = 60000;

    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_EXCEPTION", justification = "Exception detail hide")
    public static void main(String[] args) throws IOException {


        Properties properties = PropertyLoaderSingleton.getInstance().getProperties();

        String fullDBUrl = properties.getProperty("db.url") + properties.getProperty("db.name");
        ConnectionPool connectionPool = new ConnectionPool("ConnectionPool1", 10, 25, 5000, fullDBUrl, properties.getProperty("db.user"),
                properties.getProperty("db.pass"));
        SQLDBWorker workerDB = new SQLDBWorker(connectionPool, amountMilliSecondDeprecate);
        Coordinator coordinator = new Coordinator(10, 5, "Coordinator", workerDB);
        Thread threadCoordinator = new Thread(coordinator);

        threadCoordinator.start();

        try {
            Thread.sleep(500000);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }

        coordinator.setFlagStopCoordinator();
        LOGGER.info("stop coordinators");

        try {
            threadCoordinator.join();
        } catch (InterruptedException e) {
            LOGGER.error(" Sorry");
        }

        connectionPool.release();

        LOGGER.info("stop main");

    }
}
