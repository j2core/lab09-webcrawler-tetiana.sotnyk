package com.j2core.sts.webcrawler.sql.crawler.sql;

import com.j2core.sts.webcrawler.sql.dao.sql.WorkerDB;
import com.j2core.sts.webcrawler.sql.dao.sql.exception.DBException;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.ResultingInformation;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.UrlsInformation;
import org.apache.log4j.Logger;

import java.util.concurrent.*;

/**
 * Created by sts on 8/2/16.
 */

/**
 * Class create Workers threads, take URL information from DB, and saved analysed page's information in DB
 */
public class Coordinator implements Runnable{

    private static final Logger LOGGER = Logger.getLogger(Coordinator.class);                  // Object for save log information
    private BlockingQueue<UrlsInformation> pagesLink = new LinkedBlockingQueue<>();            // Collection for save page's link for process
    private BlockingQueue<ResultingInformation> analysedPages = new LinkedBlockingQueue<>();   // Collection for save result information from page's link
    private BlockingQueue<UrlsInformation> processesLink = new LinkedBlockingQueue<>();        // Collection for save link which in process
    private WorkerDB workerDB;                                                                 // Object for work with DB
    private boolean flagStopCoordinator = false;                                               // Coordinator's stopped work flag
    private static int coefficientAmountUrl = 4;                                               // Coefficient amount URL
    private final static int amountWaitTime = 500;                                             // Amount time waiting before continue work
    private final Object sync = new Object();                                                  // Object for synchronized all threads
    private int amountWorkerThreads;
    private int maxAmountTransition;
    private final String nameCoordinator;
    private Thread workMaster;
    private int nodeId;


    /**
     * Constructor Coordinator's class
     * @param amountWorkerThreads               // amount Worker's threads
     * @param maxAmountTransition               // max amount transition from first page's URL
     * @param nodeName                          // node's name
     */
    public Coordinator(int amountWorkerThreads, int maxAmountTransition, String nodeName, WorkerDB workerDB) {
        this.amountWorkerThreads = amountWorkerThreads;
        this.maxAmountTransition = maxAmountTransition;
        this.nameCoordinator = nodeName;
        this.workerDB = workerDB;
    }


    @Override
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "UW_UNCOND_WAIT", justification = "Exception detail hide")
    public void run() {

        LOGGER.info(" Start coordinator " + nameCoordinator);

        nodeId = workerDB.addNode(nameCoordinator);

        WorkMaster master = new WorkMaster(amountWorkerThreads, pagesLink, processesLink, analysedPages, maxAmountTransition, sync);
        new Thread(master).start();

        try {
            // get first link for process
            if (pagesLink.isEmpty()) {
                synchronized (sync) {
                    if (pagesLink.isEmpty()) {
                        pagesLink.addAll(workerDB.getUrlInformation(amountWorkerThreads, nodeId));
                    }
                }
            }

            while (!flagStopCoordinator){

                workerDB.returnDeprecateData();

                action();

            }

            master.setFlagStopWork(true);
            LOGGER.info("Coordinator set flag stop work");

            waitTime();

        } finally {

            if (workMaster!= null && workMaster.isAlive()) {
                try {
                    workMaster.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // save last page's information and not processed link in to data base
            workerDB.finalSaveInformation(analysedPages, processesLink, pagesLink, nodeId);

            workerDB.stopNode(nodeId);

            LOGGER.info("Coordinator finally");
        }
    }


    /**
     * The method do coordinator's work with save page's information in DB and get link for process its information from DB
     */
    private void action(){

        // save page's information from link in to data base
        int amountInformation = analysedPages.size();
        if (amountInformation > 0) {
            for (int i = 0; i < amountInformation; i++) {
                try {
                    ResultingInformation resultingInformation = analysedPages.take();
                    boolean successAddInformation = workerDB.addInformation(resultingInformation);
                    if (!successAddInformation){
                        processesLink.add(new UrlsInformation(resultingInformation.getUrlId(), resultingInformation.getPageUrl(),
                                resultingInformation.getAmountTransition(), resultingInformation.getNodeId()));
                    }
                } catch (InterruptedException | DBException e) {
                    LOGGER.error(e);
                }
            }
        }
        if (pagesLink.size() > amountWorkerThreads) {

            waitTime();

        } else {
            // get new link for process
            pagesLink.addAll(workerDB.getUrlInformation(amountWorkerThreads * coefficientAmountUrl, nodeId));
            synchronized (sync){
                sync.notifyAll();
            }
        }

    }


    /**
     *  The thread wait work
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = {"UW_UNCOND_WAIT", "WA_NOT_IN_LOOP"}, justification = "Exception detail hide")
    private void waitTime(){

        synchronized (sync){
            try {
                sync.wait(amountWaitTime);
            } catch (InterruptedException e) {
                LOGGER.error(e);
            }
        }
    }


    public void setFlagStopCoordinator(){

        this.flagStopCoordinator = true;

    }


    public boolean isFlagStopCoordinator() {
        return flagStopCoordinator;
    }

}
