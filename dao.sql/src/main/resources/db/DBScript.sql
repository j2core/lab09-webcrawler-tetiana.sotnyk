CREATE TABLE urlData      (
                                   urlId BIGINT NOT NULL AUTO_INCREMENT,
                                   url TEXT NOT NULL,
                                   amountTransition TINYINT,
                                   status ENUM('not_processed', 'processes', 'processed') DEFAULT 'not_processed',
                                   PRIMARY KEY(urlId)
                                    );

CREATE TABLE pageInformation    (
                                   pageId BIGINT NOT NULL AUTO_INCREMENT,
                                   urlId BIGINT NOT NULL,
                                   dateInformation DATE,
                                   pageText LONGTEXT CHARACTER SET utf8,
                                   PRIMARY KEY(pageId, urlId)
                                    );

CREATE TABLE wordInformation (
                                    wordId BIGINT NOT NULL AUTO_INCREMENT,
                                    pageId BIGINT NOT NULL,
                                    urlId BIGINT NOT NULL,
                                    word VARCHAR(254) NOT NULL,
                                    amountOnPage SMALLINT(5) NOT NULL,
                                    PRIMARY KEY(wordId, pageId, urlId)
                                     );


CREATE TABLE nodeData      (
                                   nodeId BIGINT NOT NULL AUTO_INCREMENT,
                                   nodeName TEXT NOT NULL,
                                   startUnixTime BIGINT,
                                   stopUnixTime BIGINT,
                                   status BIT(1),
                                   PRIMARY KEY(nodeId)
                                    );
