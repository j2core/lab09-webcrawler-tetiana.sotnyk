package com.j2core.sts.webcrawler.sql.dao.sql;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by sts on 1/4/17.
 */
@edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = {"EQ_DOESNT_OVERRIDE_EQUALS", "SE_NO_SERIALVERSIONID"}, justification = "Exception detail hide")

public class PropertyLoaderSingleton extends Properties {

    private static volatile PropertyLoaderSingleton instance = null;

    private Properties properties;
    private static final String DB_PARAMETERS = "dbparameters.properties";
    private static final String SQL_QUERY = "sqlquery.properties";

    public Properties getProperties() {
        return this.properties;
    }

    private PropertyLoaderSingleton() throws IOException {

        properties = new Properties();
        InputStream inputStreamMessageProperties = getClass().getClassLoader().getResourceAsStream(DB_PARAMETERS);
        InputStream inputStreamMessagePropertiesSQL = getClass().getClassLoader().getResourceAsStream(SQL_QUERY);

        if (inputStreamMessageProperties != null && inputStreamMessagePropertiesSQL != null) {
            properties.load(ClassLoader.getSystemResourceAsStream(DB_PARAMETERS));
            properties.load(ClassLoader.getSystemResourceAsStream(SQL_QUERY));
        }

    }

    public static PropertyLoaderSingleton getInstance() throws IOException {

        if (instance == null){
            synchronized (PropertyLoaderSingleton.class) {
                if (instance ==null) {
                    instance = new PropertyLoaderSingleton();
                }
            }
        }
        return instance;
    }

}
