package com.j2core.sts.webcrawler.sql.dao.sql.sqldao;

import com.google.common.collect.Multiset;
import com.j2core.sts.webcrawler.sql.dao.sql.PropertyLoaderSingleton;
import com.j2core.sts.webcrawler.sql.dao.sql.exception.DBException;
import com.j2core.sts.webcrawler.sql.dao.sql.WorkerDB;
import com.j2core.sts.webcrawler.sql.dao.sql.exception.NotFoundIdURLException;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.ResultingInformation;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.URLStatus;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.UrlsInformation;
import org.apache.log4j.Logger;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

import static java.sql.Statement.*;

/**
 * Created by sts on 2/16/16.
 */

/**
 * Class get URL for work from DB, added new URL in DB, added page's text and all page's words in DB, and changing URL status
 */
@edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
public class SQLDBWorker implements WorkerDB {

    private final static Logger LOGGER = Logger.getLogger(SQLDBWorker.class);                    // class for save logs information
    private final Properties selectProperties = PropertyLoaderSingleton.getInstance().getProperties(); // properties object with selects to the DB
    private final ConnectionPool connectionPool;
    private final int amountMilliSecondDeprecate;

    /**
     * Constructor WorkerDB's class
     *
     * @param connectionPool connection Pool for work with DB
     * @param amountMilliSecondDeprecate   amount milliSec
     */
    public SQLDBWorker(ConnectionPool connectionPool, int amountMilliSecondDeprecate) throws IOException {

        this.connectionPool = connectionPool;
        this.amountMilliSecondDeprecate = amountMilliSecondDeprecate;

    }


    /**
     * The method get URL's information from the data base
     *
     * @param amountUrl     amount URL
     * @return         collection with URL's information
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "REC_CATCH_EXCEPTION", justification = "Exception detail hide")
    public List<UrlsInformation> getUrlInformation(int amountUrl, int nodeId) {

        List<UrlsInformation> urlInformationList = new LinkedList<>();
        int id;
        String url;
        int amountTransition;
        String selectUrlInformation = selectProperties.getProperty("getURLLock");

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(selectUrlInformation)) {

            statement.setInt(1, amountUrl);

            try (ResultSet setInformation = statement.executeQuery()) {

                while (setInformation.next()) {
                    id = setInformation.getInt("urlId");
                    url = setInformation.getString("url");
                    amountTransition = setInformation.getInt("amountTransition");
                    urlInformationList.add(new UrlsInformation(id, url, amountTransition, -1));
                }
            }

            if (!urlInformationList.isEmpty() && !changeDateGetUrl(dbConnection, urlInformationList, nodeId)) throw new SQLException();

        } catch (SQLException ex) {
            LOGGER.error("Transaction failed", ex);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return urlInformationList;
    }


    /**
     * The method change url's data after get from DB for work
     *
     * @param connection              connection to the DB
     * @param urlsInformationList     collection with URL information
     * @param nodeId                  node's Id
     * @return change successfully or not
     */
    private boolean changeDateGetUrl(Connection connection, List<UrlsInformation> urlsInformationList, int nodeId){

        String selectUpdate = selectProperties.getProperty("updateGetUrl");

        for (UrlsInformation url : urlsInformationList) {

            try (PreparedStatement statement = connection.prepareStatement(selectUpdate)) {

                statement.setInt(1, nodeId);
                statement.setInt(2, URLStatus.PROCESSES.getStatusNumber());
                statement.setLong(3, System.currentTimeMillis());
                statement.setInt(4, url.getId());

                statement.executeUpdate();

            } catch (SQLException e) {
                LOGGER.error(e);
                return false;
            }
        }

        return true;
    }


    /**
     * The method added new URL information in DB
     *
     * @param information   page's information
     * @return added successfully or no
     */
    public boolean addUrlInformation(ResultingInformation information)  {

        String selectUrlInformation = selectProperties.getProperty("getAddURL");
        String selectInsert = selectProperties.getProperty("addURL");

        Set<UrlsInformation> collectionUrlsInformation = information.getUrlCollectionNew();
        if (collectionUrlsInformation != null) {

            try(Connection dbConnection = connectionPool.getConnection(); PreparedStatement statementSelectUrl = dbConnection.prepareStatement(selectUrlInformation);
            PreparedStatement statementInsertUrl = dbConnection.prepareStatement(selectInsert)) {

                for (UrlsInformation urlInformation : collectionUrlsInformation) {

                    statementSelectUrl.setString(1, urlInformation.getPagesUrl());

                    try(ResultSet setInformation = statementSelectUrl.executeQuery()) {

                        if (!setInformation.next()) {

                            statementInsertUrl.setString(1, urlInformation.getPagesUrl());
                            statementInsertUrl.setInt(2, urlInformation.getAmountTransition());
                            statementInsertUrl.setLong(3, System.currentTimeMillis());
                            statementInsertUrl.executeUpdate();
                        }
                    }
                }
            } catch (SQLException e) {
                LOGGER.error(e);
                return false;
            }
        }
        return true;
    }


    /**
     * The method added page's information on the data base
     *
     * @param information   page's information
     * @return successfully added or not
     */
    public boolean addInformation(ResultingInformation information) throws DBException {

        String selectPagesInformation = selectProperties.getProperty("getAddInformation");

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statementUrl = dbConnection.prepareStatement(selectPagesInformation)) {

            dbConnection.setAutoCommit(false);

            LOGGER.info(information.getPageUrl());

            statementUrl.setString(1, information.getPageUrl());

            try (ResultSet resultSet = statementUrl.executeQuery()) {

                if (!resultSet.next()) {
                    throw new NotFoundIdURLException(" Repeat data");
                }

                addUrlInformation(information);

                if (addPageInformation(information)) {     // NOPMD - this statements was not  combined for readability

                    if (addPageWords(information)) {          // NOPMD - this statements was not  combined for readability

                        changeURLStatus(URLStatus.PROCESSED, information.getUrlId());      // NOPMD - this statements was not  combined for readability
                    }
                }
            }

            dbConnection.commit();

        } catch (SQLException ex) {
            LOGGER.error("Transaction is failed", ex);
            return false;
        }
        return true;
    }

    @Override
    public int addNode(String name) {

        int nodeId = 0;
        String selectStartNode = selectProperties.getProperty("startNode");

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement =
                dbConnection.prepareStatement(selectStartNode, RETURN_GENERATED_KEYS)) {

            LOGGER.info(" Create new node. Node's name is " + name);

            dbConnection.setAutoCommit(false);

            statement.setString(1, name);
            statement.setLong(2, System.currentTimeMillis());
            statement.setLong(3, 0);
            statement.setBoolean(4, true);

            statement.executeUpdate();

            try (ResultSet resultSet = statement.getGeneratedKeys()){

                if (resultSet.next()){

                    nodeId = resultSet.getInt(1);
                }

            }

            dbConnection.commit();

        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return nodeId;
    }

    @Override
    public void stopNode(int nodeId) {

        String selectStopNode = selectProperties.getProperty("stopNode");

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement =
                dbConnection.prepareStatement(selectStopNode)) {

            LOGGER.info(" Stop node. Node's Id is " + nodeId);

            dbConnection.setAutoCommit(false);

            statement.setLong(1, System.currentTimeMillis());
            statement.setBoolean(2, false);
            statement.setInt(3, nodeId);

            statement.executeUpdate();

            dbConnection.commit();

        } catch (SQLException e) {
            LOGGER.error(e);
        }

    }

    @Override
    public boolean returnDeprecateData() {

        long unixTime = System.currentTimeMillis();
        long unixTimeOld = unixTime - amountMilliSecondDeprecate;

        String returnUrl = selectProperties.getProperty("returnUrl");
        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(returnUrl)) {

            statement.setLong(1, unixTimeOld);

            dbConnection.setAutoCommit(false);

            statement.executeUpdate();

            dbConnection.commit();

        } catch (SQLException e) {
            LOGGER.error("Return url been unsuccessful " + e);
            return false;
        }
        return true;

    }


    /**
     * The method change URL's status if need change only one status
     *
     * @param status         status for changing
     * @param urlId             URL which status need change
     * @return   change status successfully or no
     */
    public boolean changeURLStatus(URLStatus status, int urlId){

        String selectUrlStatus = selectProperties.getProperty("selectURLStatus");
        String selectUpdateStatus = selectProperties.getProperty("updateURLStatus");

        try(Connection dbConnection = connectionPool.getConnection(); PreparedStatement statementLock = dbConnection.prepareStatement(selectUrlStatus);
            PreparedStatement statementUpdateStatus = dbConnection.prepareStatement(selectUpdateStatus)) {

            statementLock.setInt(1, urlId);

            statementUpdateStatus.setInt(1, status.getStatusNumber());
            statementUpdateStatus.setLong(2, System.currentTimeMillis());
            statementUpdateStatus.setInt(3, urlId);

            try(ResultSet resultSet = statementLock.executeQuery()) {
                if (resultSet.next()) {
                    statementUpdateStatus.executeUpdate();
                }
            }

        } catch (SQLException e) {
            LOGGER.error("Change page's status been unsuccessful " + urlId, e);
            return false;
        }
        return true;

    }


    /**
     * The method change URL status
     *
     * @param status         status for changing
     * @param information    collection with URL's ids
     * @return  change status successfully or no
     */
    public boolean changeURLStatus(URLStatus status, List<UrlsInformation> information){

        if (information.size() > 0) {

            String selectUrlStatus = createSelectLockChangeStatus(information);
            String selectUpdateStatus = createSelectUpdateStatus(information);

            try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statementLock = dbConnection.prepareStatement(selectUrlStatus);
                 PreparedStatement statementUpdateStatus = dbConnection.prepareStatement(selectUpdateStatus)) {

                statementUpdateStatus.setInt(1, status.getStatusNumber());

                dbConnection.setAutoCommit(false);

                try (ResultSet resultSet = statementLock.executeQuery()) {
                    LOGGER.info(" Take Lock ");

                    if (resultSet.next()) {
                        statementUpdateStatus.executeUpdate();
                        LOGGER.info(" GOOD");
                    }
                }

                dbConnection.commit();

            } catch (SQLException e) {
                LOGGER.error("Change page's status been unsuccessful sqlArray" + selectUrlStatus + selectUpdateStatus, e);
                return false;
            }
        }
        return true;
    }


    /**
     * The method create SQL select with update URL's status where many ids in one time
     *
     * @param information     collection with id's values
     * @return  select to the update URL's status
     */
    private String createSelectUpdateStatus(List<UrlsInformation> information) {

        StringBuilder select = new StringBuilder();
        int index = information.size();

        select.append("UPDATE urlData SET status = ?, statusChangeTime = ").append(System.currentTimeMillis()).append(" WHERE urlId IN ( ");

        for (UrlsInformation url : information){
            select.append(url.getId());
            if (index > 1){
                select.append(", ");
                index--;
            }
        }

        select.append(")");

        return select.toString();
    }


    /**
     * The method added information from page in DB
     *
     * @param information   page's information which add in DB
     * @return added information successfully or no
     */
    public boolean addPageInformation(ResultingInformation information){

        String selectInsert = selectProperties.getProperty("addPageInformation");

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statementInsertPagesInformation = dbConnection.prepareStatement(selectInsert, RETURN_GENERATED_KEYS)){

            statementInsertPagesInformation.setInt(1, information.getUrlId());
            statementInsertPagesInformation.setDate(2, new Date(new java.util.Date().getTime()));
            statementInsertPagesInformation.setString(3, information.getPagesText());
            statementInsertPagesInformation.executeUpdate();

            try(ResultSet result = statementInsertPagesInformation.getGeneratedKeys()) {
                if (result.next()) {
                    information.setPageId(result.getInt(1));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Adding page's information been unsuccessful", e);
            return false;
        }
        return true;
    }


    /**
     * The method added all page's words in DB
     *
     * @param information   all page's words for added in DB
     * @return added word's successfully or no
     */
    public boolean addPageWords(ResultingInformation information) {

        String selectInsert = selectProperties.getProperty("addPageWord");

        if (information.getWordsInPage().size() > 0) {
            Set<Multiset.Entry<String>> wordsCollection = information.getWordsInPage().entrySet();

            try(Connection dbConnection = connectionPool.getConnection(); PreparedStatement statementInsertWords = dbConnection.prepareStatement(selectInsert)) {

                for (Multiset.Entry<String> word : wordsCollection) {

                    statementInsertWords.setInt(1, information.getPageId());
                    statementInsertWords.setString(2, information.getPageUrl());
                    statementInsertWords.setString(3, word.getElement());
                    statementInsertWords.setInt(4, word.getCount());
                    statementInsertWords.executeUpdate();
                }

            } catch (SQLException e) {
                LOGGER.error("Adding page's words been unsuccessful", e);
                return false;
            }
        }
        return true;
    }


    /**
     * The method create SQL select for lock data with collection value
     *
     * @param information  collection with values
     * @return  select to the DB
     */
    private String createSelectLockChangeStatus(List<UrlsInformation> information){

        StringBuilder select = new StringBuilder();

        int index = information.size();
        select.append("SELECT * FROM urlData WHERE urlId IN ( ");

        for (UrlsInformation url : information){
            select.append(url.getId());
            if (index > 1){
                select.append(", ");
                index--;
            }
        }

        select.append(") LOCK IN SHARE MODE");

        return select.toString();
    }


    /**
     * The method save finished analysed URL and not analysed URL in DB
     *
     * @param analysedPages   collection with analysed pages
     * @param pagesLink       collection with not processed URL
     */
    public void finalSaveInformation(BlockingQueue<ResultingInformation> analysedPages, BlockingQueue<UrlsInformation> processesLink, BlockingQueue<UrlsInformation> pagesLink, int nodeId) {

        if (!processesLink.isEmpty()) {
            pagesLink.addAll(processesLink);
        }
        if (!analysedPages.isEmpty()) {
            try {

                for (ResultingInformation resultingInformation : analysedPages) {
                    addInformation(resultingInformation);
                }

            } catch (DBException e) {
                LOGGER.error(e);
            }
        }
        if (!pagesLink.isEmpty()){

            changeURLStatus(URLStatus.NOT_PROCESSED, changeCollection(pagesLink));

        }
    }


    /**
     * The method change blocking queue to the list
     *
     * @param pagesLink      blocking queue collection
     * @return              new list collection
     */
    private List<UrlsInformation> changeCollection (BlockingQueue<UrlsInformation> pagesLink){

        List<UrlsInformation> list = new LinkedList<>();

        for (UrlsInformation urlsInformation : pagesLink){

            list.add(urlsInformation);

        }
        return list;
    }

}