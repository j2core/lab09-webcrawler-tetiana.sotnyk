package com.j2core.sts.webcrawler.sql.dao.sql;

import com.j2core.sts.webcrawler.sql.dao.sql.sqldao.SQLDBWorker;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.PagesAnalyser;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.ResultingInformation;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.URLStatus;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.UrlsInformation;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by sts on 1/12/17.
 */
public class TestSQLDBWorker {

    private Properties selectProperties;
    private static ConnectionPool connectionPool;
    private final static int amountMilliSecondDeprecate = 60000;

    @BeforeClass
    public void createTestDB() throws IOException {

        selectProperties = PropertyLoaderSingleton.getInstance().getProperties(); // properties object with selects to the DB
        String fullDBUrl = selectProperties.getProperty("db.url") + selectProperties.getProperty("db.name");
        connectionPool = new ConnectionPool("ConnectionPool1", 10, 25, 5000, fullDBUrl, selectProperties.getProperty("db.user"),
                selectProperties.getProperty("db.pass"));

    }


    @Test
    public void testStartNode() throws IOException {

        String nodeName = "Node test1";
        String result = null;
        SQLDBWorker worker = new SQLDBWorker(connectionPool, amountMilliSecondDeprecate);

        int newNodeId = worker.addNode(nodeName);

        String selectNode = selectProperties.getProperty("getNodeId");

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement =
                dbConnection.prepareStatement(selectNode)) {

            statement.setInt(1, newNodeId);

            try (ResultSet resultSet = statement.executeQuery()){

                if (resultSet.next()){

                    result = resultSet.getString("nodeName");
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(result, nodeName);

    }


    @Test
    public void testStopNode() throws IOException {

        String nodeName = "Node test2";
        SQLDBWorker worker = new SQLDBWorker(connectionPool, amountMilliSecondDeprecate);
        String getNodeId = selectProperties.getProperty("getNodeId");
        int nodeId = worker.addNode(nodeName);
        boolean result = true;

        worker.stopNode(nodeId);

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement =
                dbConnection.prepareStatement(getNodeId)) {

            statement.setInt(1, nodeId);

            try (ResultSet resultSet = statement.executeQuery()){

                if (resultSet.next()){

                    result = resultSet.getBoolean("status");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertFalse(result);

    }


    @Test
    public void testGetUrlInformation() throws IOException {

        String nodeName = "Node test3";
        SQLDBWorker worker = new SQLDBWorker(connectionPool, amountMilliSecondDeprecate);
        int nodeId = worker.addNode(nodeName);
        int amountUrl = 5;
        List<UrlsInformation> urlInformationList;
        List<UrlsInformation> result = null;
        String getNodeURL = selectProperties.getProperty("getNodeURL");

        urlInformationList = worker.getUrlInformation(amountUrl, nodeId);

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(getNodeURL)) {

            statement.setInt(1, nodeId);

            try (ResultSet setInformation = statement.executeQuery()) {

                result = createCollectionUrlInformation(setInformation);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(result.size(), amountUrl);

    }


    @Test
    public void testChangeURLStatusSingl() throws IOException {

        SQLDBWorker worker = new SQLDBWorker(connectionPool, amountMilliSecondDeprecate);
        String getURL = selectProperties.getProperty("getURLLock");
        String getStatus = selectProperties.getProperty("getUrl");
        int urlId = 0;
        int status = -1;

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(getURL);
             PreparedStatement preparedStatement = dbConnection.prepareStatement(getStatus)) {

            statement.setInt(1, 1);

            try (ResultSet setInformation = statement.executeQuery()) {

                if (setInformation.next()) {
                    urlId = setInformation.getInt("urlId");
                }
            }

            dbConnection.setAutoCommit(false);

            worker.changeURLStatus(URLStatus.PROCESSES, urlId);

            dbConnection.commit();

            preparedStatement.setInt(1, urlId);

            try (ResultSet resultUrl = preparedStatement.executeQuery()){

                if (resultUrl.next()){

                    status = resultUrl.getInt("status");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(URLStatus.PROCESSES.getStatusNumber(), status);

    }


    @Test
    public void testChangeURLStatusList() throws IOException {

        SQLDBWorker worker = new SQLDBWorker(connectionPool, amountMilliSecondDeprecate);
        int amountStatusBefore = -1;
        int amountStatusAfter = 0;
        int amountUrl = 3;
        String getUrl = selectProperties.getProperty("getUrlLimit");
        String getStatusUrl = selectProperties.getProperty("countStatusUrl");
        List<UrlsInformation> urlList;

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(getUrl);
             PreparedStatement preparedStatement = dbConnection.prepareStatement(getStatusUrl)) {

            statement.setInt(1, URLStatus.NOT_PROCESSED.getStatusNumber());
            statement.setInt(2, amountUrl);
            preparedStatement.setInt(1, URLStatus.PROCESSES.getStatusNumber());

            try (ResultSet getUrlList = statement.executeQuery(); ResultSet amountUrlStatus = preparedStatement.executeQuery()) {

                urlList = createCollectionUrlInformation(getUrlList);
                while (amountUrlStatus.next()){
                    amountStatusBefore = amountUrlStatus.getInt(1);
                }
            }

            worker.changeURLStatus(URLStatus.PROCESSES, urlList);

            try (ResultSet resultSet = preparedStatement.executeQuery()){

                while (resultSet.next()){
                    amountStatusAfter = resultSet.getInt(1);
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertEquals((amountStatusBefore + amountUrl), amountStatusAfter);

    }


    private List<UrlsInformation> createCollectionUrlInformation(ResultSet resultSet) throws SQLException {

        List<UrlsInformation> urlList = new LinkedList<>();

        while (resultSet.next()) {
            int id = resultSet.getInt("urlId");
            String url = resultSet.getString("url");
            int amountTransition = resultSet.getInt("amountTransition");
            urlList.add(new UrlsInformation(id, url, amountTransition, -1));
        }

        return urlList;

    }

    private ResultingInformation createResultInformation(UrlsInformation urlsInformation){

        PagesAnalyser pagesAnalyser = new PagesAnalyser(5, 5);

        return pagesAnalyser.analysePageInformation(urlsInformation);
    }


    @Test
    public void testAddPageInformation() throws IOException {

        SQLDBWorker worker = new SQLDBWorker(connectionPool, amountMilliSecondDeprecate);
        int nodeId = worker.addNode("Node test5");
        UrlsInformation urlsInformation = worker.getUrlInformation(1, nodeId).get(0);
        ResultingInformation resultingInformation = createResultInformation(urlsInformation);
        int amountPageBefore = -1;
        int amountPageAfter = 0;

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(
                selectProperties.getProperty("counterPage"))) {

            try (ResultSet resultSet = statement.executeQuery()){

                while (resultSet.next()){
                    amountPageBefore = resultSet.getInt(1);
                }
            }

            dbConnection.setAutoCommit(false);

            worker.addPageInformation(resultingInformation);

            dbConnection.commit();

            try (ResultSet resultSet = statement.executeQuery()){

                while (resultSet.next()){
                    amountPageAfter = resultSet.getInt(1);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertEquals((amountPageBefore+1), amountPageAfter);

    }


    @Test
    public void testAddUrlInformation() throws IOException {

        SQLDBWorker worker = new SQLDBWorker(connectionPool, amountMilliSecondDeprecate);
        int nodeId = worker.addNode("Node test5");
        UrlsInformation urlsInformation = worker.getUrlInformation(1, nodeId).get(0);
        ResultingInformation resultingInformation = createResultInformation(urlsInformation);
        int amountPageBefore = -1;
        int amountPageAfter = 0;

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(
                selectProperties.getProperty("counterUrl"))) {

            try (ResultSet resultSet = statement.executeQuery()){

                while (resultSet.next()){
                    amountPageBefore = resultSet.getInt(1);
                }
            }

            dbConnection.setAutoCommit(false);

            worker.addUrlInformation(resultingInformation);

            dbConnection.commit();

            try (ResultSet resultSet = statement.executeQuery()){

                while (resultSet.next()){
                    amountPageAfter = resultSet.getInt(1);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertTrue(amountPageBefore < amountPageAfter);

    }


    @Test
    public void testAddPageWords() throws IOException {

        SQLDBWorker worker = new SQLDBWorker( connectionPool, amountMilliSecondDeprecate);
        int nodeId = worker.addNode("Node test6");
        List<UrlsInformation> list;
        do {
            list = worker.getUrlInformation(1, nodeId);
        }
        while (list.isEmpty());

        UrlsInformation urlsInformation = list.get(0);
        ResultingInformation resultingInformation = createResultInformation(urlsInformation);
        int amountPageBefore = -1;
        int amountPageAfter = 0;

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(
                selectProperties.getProperty("counterWords"))) {

            try (ResultSet resultSet = statement.executeQuery()){

                while (resultSet.next()){
                    amountPageBefore = resultSet.getInt(1);
                }
            }
            dbConnection.setAutoCommit(false);

            worker.addPageInformation(resultingInformation);

            dbConnection.commit();

            dbConnection.setAutoCommit(false);

            worker.addPageWords(resultingInformation);

            dbConnection.commit();

            try (ResultSet resultSet = statement.executeQuery()){

                while (resultSet.next()){
                    amountPageAfter = resultSet.getInt(1);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        int addedWords = resultingInformation.getWordsInPage().entrySet().size();

        Assert.assertEquals((amountPageBefore + addedWords), amountPageAfter);

    }


    @Test
    public void testFinalSaveInformation() throws IOException {

        int amountUrlAfter = 0;
        int amountUrlBefore = 0;
        SQLDBWorker worker = new SQLDBWorker( connectionPool, amountMilliSecondDeprecate);
        int nodeId = worker.addNode("Node test7");

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(
                selectProperties.getProperty("countStatusUrl"))) {

            statement.setInt(1, 1);

            try (ResultSet resultSet = statement.executeQuery()){

                while (resultSet.next()){
                    amountUrlBefore = resultSet.getInt(1);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        BlockingQueue<UrlsInformation> urlCollection = new LinkedBlockingQueue<>();
        List<UrlsInformation> urlsInformationList = worker.getUrlInformation(11, nodeId);

        for (UrlsInformation url : urlsInformationList){
            urlCollection.add(url);
        }

        worker.finalSaveInformation(new LinkedBlockingQueue<ResultingInformation>(), new LinkedBlockingQueue<UrlsInformation>(), urlCollection, nodeId);

        try (Connection dbConnection = connectionPool.getConnection(); PreparedStatement statement = dbConnection.prepareStatement(
                selectProperties.getProperty("countStatusUrl"))) {

            statement.setInt(1, 1);

            try (ResultSet resultSet = statement.executeQuery()){

                while (resultSet.next()){
                    amountUrlAfter = resultSet.getInt(1);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(amountUrlBefore, amountUrlAfter);

    }

}
