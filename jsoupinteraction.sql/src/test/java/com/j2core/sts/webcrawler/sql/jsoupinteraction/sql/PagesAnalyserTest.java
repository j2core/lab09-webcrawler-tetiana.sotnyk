package com.j2core.sts.webcrawler.sql.jsoupinteraction.sql;

import com.google.common.collect.Multiset;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.UrlsInformation;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Set;

/**
 * Created by sts on 1/13/16.
 */
public class PagesAnalyserTest {


    @Test
    public void testTakePagesDomain(){

        PagesAnalyser analyse = new PagesAnalyser(20, 5);

        String domain = analyse.takePageDomain("http://www.quizful.net/post/Crawling-Java");

        Assert.assertTrue(domain.equals("http://www.quizful.net"));

    }

    @Test(enabled = false)
    public void testReceiveLinksFromPagePositive() throws IOException {

        PagesAnalyser analyse = new PagesAnalyser(20, 5);
        String page = "http://www.quizful.net/post/Crawling-Java";

        Set<UrlsInformation> newPagesLink = analyse.receiveLinksFromPage(Jsoup.connect(page).get(), page, 0);

        Assert.assertEquals(newPagesLink.size(), 31);

    }

    @Test(expectedExceptions = IOException.class)
    public void testReceiveLinksFromPageNegative() throws IOException {

        PagesAnalyser analyse = new PagesAnalyser(20, 5);
        String page = "http://www.ksrhfkjhb.com";

        Set<UrlsInformation> newPagesLink = analyse.receiveLinksFromPage(Jsoup.connect(page).get(), page, 0);

    }


    @Test
    public void testParsePageContent() throws IOException {

        PagesAnalyser analyse = new PagesAnalyser(20, 5);
        String page = "http://www.quizful.net/post/Crawling-Java";
        Document document = Jsoup.connect(page).get();

        Multiset<String> wordInSite = analyse.parsePageContent(document);

        Assert.assertFalse(wordInSite.isEmpty());

    }

}
