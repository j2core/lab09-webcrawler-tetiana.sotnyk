package com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto;

/**
 * Created by sts on 2/12/16.
 */

/**
 * Class for save information about page's URL
 */
public class UrlsInformation {

    private final String pagesUrl;
    private final int id;
    private int amountTransition;
    private int amountReadPage = 0; // amount read page
    private URLStatus status = URLStatus.NOT_PROCESSED;
    private int nodeId;

    /**
     * Constructor UrlInformation's class
     *
     * @param id               URL's Id
     * @param pagesUrl         page's URL
     * @param amountTransition amount external hops from the starting point
     */
    public UrlsInformation(int id, String pagesUrl, int amountTransition, int nodeId) {
        this.pagesUrl = pagesUrl;
        this.id = id;
        this.amountTransition = amountTransition;
        this.nodeId = nodeId;
    }

    public void setAmountTransition(int amountTransition) {
        this.amountTransition = amountTransition;
    }

    public int getId() {
        return id;
    }

    public String getPagesUrl() {
        return pagesUrl;
    }

    public int getAmountTransition() {
        return amountTransition;
    }

    public int getAmountReadPage() {
        return amountReadPage;
    }

    public URLStatus getStatus() {
        return status;
    }

    public void setStatus(URLStatus status) {
        this.status = status;
    }

    public void setAmountReadPage(int amountReadPage) {
        this.amountReadPage = amountReadPage;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * Method changed value counter amount read page
     */
    public void changeAmountReadPage(){
        amountReadPage++;
    }


    @Override
    public String toString() {
        return "UrlsInformation{" +
                "pagesUrl='" + pagesUrl + '\'' +
                ", id=" + id +
                ", amountTransition=" + amountTransition +
                ", amountReadPage=" + amountReadPage +
                ", status=" + status +
                '}';
    }
}
