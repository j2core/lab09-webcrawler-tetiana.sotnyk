package com.j2core.sts.webcrawler.sql.jsoupinteraction.sql;

/**
 * Created by sts on 1/20/16.
 */

import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.ResultingInformation;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.UrlsInformation;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.exception.NotReadPageException;

/**
 * Interface for analyse page's information
 */
public interface WebInteraction {

    /**
     * Method for analyse page's information from URL
     *
     * @param pagesUrl  page's URL for analyse information
     * @return Page's information
     */
    ResultingInformation analysePageInformation(UrlsInformation pagesUrl) throws NotReadPageException;
}
