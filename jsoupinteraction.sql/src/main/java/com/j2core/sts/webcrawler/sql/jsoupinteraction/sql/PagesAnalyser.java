package com.j2core.sts.webcrawler.sql.jsoupinteraction.sql;

import com.google.common.base.Splitter;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.ResultingInformation;
import com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto.UrlsInformation;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by sts on 1/12/16.
 */

/**
 * Class for analyse page's information. This class does Receive links from page and parse page's content.
 */
public class PagesAnalyser implements WebInteraction{

    private static final Logger LOGGER = Logger.getLogger(PagesAnalyser.class);  // object for logging this class
    private int maxAmountTransition;                                             // max amount transition from first page's URL
    private final int valueReadPage;                                             // amount of pages read attempts

    /**
     * Constructor
     *
     * @param maxAmountTransition     max value amount transition on the original page
     * @param amountReadPage          amount endeavour read page, if its not access
     */
    public PagesAnalyser(int maxAmountTransition, int amountReadPage) {
        this.maxAmountTransition = maxAmountTransition;
        this.valueReadPage = amountReadPage;
    }


    @Override
    public ResultingInformation analysePageInformation(UrlsInformation urlsInformation) {

        ResultingInformation pageInformation = null;

        if (urlsInformation.getAmountTransition() < maxAmountTransition) {

            if (urlsInformation.getAmountReadPage() < valueReadPage) {

                Document htmlDocument;
                pageInformation = new ResultingInformation(urlsInformation.getId(), urlsInformation.getPagesUrl(),
                        urlsInformation.getAmountTransition(), urlsInformation.getNodeId());

                try {
                    htmlDocument = Jsoup.connect(urlsInformation.getPagesUrl()).get();

                    if (urlsInformation.getAmountTransition() < maxAmountTransition) {
                        pageInformation.setUrlCollectionNew(receiveLinksFromPage(htmlDocument, urlsInformation.getPagesUrl(),
                                urlsInformation.getAmountTransition()));
                    }
                    pageInformation.setPagesText(htmlDocument.body().text());

                    pageInformation.setWordsInPage(parsePageContent(htmlDocument));

                } catch (Exception exp) {
                    urlsInformation.changeAmountReadPage();
                    if (urlsInformation.getAmountReadPage() == valueReadPage) {
                        urlsInformation.setAmountReadPage(-1);
                    }
                    LOGGER.error(exp);
                    return null;
                }
            }

        }

        return pageInformation;
    }


    /**
     * This method is receiving all links from page
     *
     * @param htmlDocument      data from page
     * @param pageUrl           page's URL
     * @param amountTransition  value amount transition of original page
     * @return collection with all links from page
     */
    public Set<UrlsInformation> receiveLinksFromPage(Document htmlDocument, String pageUrl, int amountTransition){


        Set<UrlsInformation> collectionNewUrl = new HashSet<UrlsInformation>();

        String domainUrl = takePageDomain(pageUrl);

        Elements elements = htmlDocument.getElementsByTag("a");

        for(Element elem : elements){
            String newUrl = elem.toString();

            int index = newUrl.indexOf("href=\"")+6;
            try {
                newUrl = newUrl.substring(index, newUrl.indexOf('\"', index));
            }catch (StringIndexOutOfBoundsException e){
                LOGGER.error(e);
            }


            if (!newUrl.contains("http")){
                newUrl = domainUrl + newUrl;
            }

            collectionNewUrl.add(new UrlsInformation(-1, newUrl, (amountTransition + 1), -1));
        }

        return collectionNewUrl;
    }


    /**
     * This method is parsing page's content
     *
     * @param htmlDocument  data from page
     * @return  collection with all words from page and their amount on this page
     */
    public Multiset<String> parsePageContent(Document htmlDocument){

        Multiset<String> wordInSite = HashMultiset.create();
        String text = htmlDocument.body().text();

        wordInSite.addAll(Splitter.onPattern("([,.:@!?;1234567890()<>/{}+-=*&#$%•]|\\[|\\]|\\s)").trimResults().omitEmptyStrings().splitToList(text));

        return wordInSite;

    }


    /**
     * This method is taking domain from URL
     *
     * @param pagesUrl     string with page's URL
     * @return  string with page's domain
     */
    public String takePageDomain(String pagesUrl){

        if (pagesUrl.indexOf("/", 8) < 0) {
            return pagesUrl;
        }else return pagesUrl.substring(0, pagesUrl.indexOf("/", 8));
    }

    public int getMaxAmountTransition() {
        return maxAmountTransition;
    }

    public void setMaxAmountTransition(int maxAmountTransition) {
        this.maxAmountTransition = maxAmountTransition;
    }

    public int getValueReadPage() {
        return valueReadPage;
    }

}
