package com.j2core.sts.webcrawler.sql.jsoupinteraction.sql.dto;

import com.google.common.collect.Multiset;

import java.util.Set;

/**
 * Created by sts on 1/20/16.
 */

/**
 * Object for saves information about page
 */
public class ResultingInformation {

    private final String pageUrl;
    private int amountTransition;
    private Multiset<String> wordsInPage = null;           // collection for saved all word from this page and their amount on this page
    private Set<UrlsInformation> urlCollectionNew = null;  // collection for saved all links from this page
    private String pagesText;                              // page's content
    private final int urlId;                              // URL's id in DB
    private int pageId;                                  // Page's id in DB
    private int nodeId;


    /**
     * Constructor  ResultingInformation's class
     *
     * @param urlId                 id URL's in DB
     * @param pageUrl            page's URL
     * @param amountTransition   amount transition from original page
     * @param nodeId             node's id
     */
    public ResultingInformation(int urlId, String pageUrl, int amountTransition, int nodeId) {
        this.urlId = urlId;
        this.pageUrl = pageUrl;
        this.amountTransition = amountTransition;
        this.nodeId = nodeId;

    }
    

    public Multiset<String> getWordsInPage() {
        return wordsInPage;
    }

    public void setWordsInPage(Multiset<String> wordsInPage) {
        this.wordsInPage = wordsInPage;
    }

    public Set<UrlsInformation> getUrlCollectionNew() {
        return urlCollectionNew;
    }

    public void setUrlCollectionNew(Set<UrlsInformation> urlCollectionNew) {
        this.urlCollectionNew = urlCollectionNew;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public int getAmountTransition() {
        return amountTransition;
    }

    public String getPagesText(){
        return pagesText;
    }

    public void setPagesText(String text){
        pagesText = text;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public int getUrlId() {
        return urlId;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    @Override
    public String toString() {
        return "ResultingInformation{" +
                "urlId=" + urlId +
                "pageUrl='" + pageUrl + '\'' +
                ", amountTransition=" + amountTransition +
                ", wordsInPage=" + wordsInPage +
                ", urlCollectionNew=" + urlCollectionNew +
                ", pagesText='" + pagesText + '\'' +
                '}';
    }
}
